﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace NotebookApp
{
    class Notebook
    {
        public Notebook()
        {
            Notes = new List<Note>();
        }

        public void CreateNote(string name, string lastname, string country, 
                               long telephoneNumber, List<string> optional = null)
        {
            Note note = new Note (
                                 new Note.FirstName(name),
                                 new Note.LastName(lastname),
                                 new Note.PersonCountry(country),
                                 new Note.Telephone(telephoneNumber)
                                 );
            note.AddOptionalFields(optional);
            Notes.Add(note);
        }
       
        public void EditNote(int indexOfNote, int indexOfNewField, string newField)
        {
            Note note = Notes.ElementAt(indexOfNote);
            note.SetNoteField(indexOfNewField, newField);
            Notes[indexOfNote] = note;
        }

        public void DeleteNote(int id)
        {
            Notes.RemoveAt(id);
        }
        public Note GetNoteAtIndex(int id)
        {
            return Notes.ElementAt(id);
        }
        public int DeleteAllNotes()
        {
            int count = Notes.Count;
            Notes.Clear();
            return count;
        }
        public List<Note> Notes { get; private set; }
    }
}
