﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using static System.Console;

namespace NotebookApp
{
        partial class UserInterface
        {

        static void Main(string[] args)
        {
            MainEventLoop();
        }

        private Notebook book;

        public UserInterface()
        {
            book = new Notebook();
        }

        static void MainEventLoop()
        {
            WriteLine("\t\t\t\t---Записная книжка v1.0---");
            WriteLine("Вы можете:\nсоздавать(create)\nпросматривать(show, showAll)\n" +
                "редактировать(edit)\nудалять(del, delAll)\nзаписи в записной книжке");
            WriteLine("\nВведите help, чтобы вызвать справку");
            WriteLine("Введите exit, чтобы выйти\n");
            UserInterface ui = new UserInterface();

            while (true)
            {
                WriteLine("Введите команду:");
                string answer = ReadLine();
                bool exitFlag = false;

                switch (answer)
                {
                    case "create":
                        ui.Create();
                        break;
                    case "edit":
                        ui.Edit();
                        break;
                    case "del":
                        ui.Delete();
                        break;
                    case "delAll":
                        ui.DeleteAll();
                        break;
                    case "show":
                        ui.Show();
                        break;
                    case "showAll":
                        ui.ShowAll();
                        break;
                    case "exit":
                        exitFlag = true;
                        break;
                    case "help":
                        ui.Help();
                        break;
                    default:
                        break;
                }
                if (exitFlag)
                    break;
            }
            return;
        }
    }
}
