﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Note : object
    {
        //obligatory fields
        public FirstName Firstname { get; set; }
        public LastName Lastname { get; set; }
        public PersonCountry Country { get; set; }
        public Telephone TelephonNo { get; set; }

        //optional fields
        public string DateBirth { get; set; }
        public string SecondName { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string AdditionalInfo { get; set; }


        public Note(FirstName name, LastName lastName, PersonCountry country, Telephone tel)
        {
            Firstname = name;
            Lastname = lastName;
            Country = country;
            TelephonNo = tel;
        }

        public void AddOptionalFields(List<string> optional = null)
        {
            DateBirth = optional[0];
            SecondName = optional[1];
            Organization = optional[2];
            Position = optional[3];
            AdditionalInfo = optional[4];
        }

        public override string ToString()
        {
            return "first name: " + Firstname.record +
                   "\nsecond name: " + Lastname.record +
                   "\ntel.: " + TelephonNo.record + "\n";
        }

        public string GetFullNoteInfo()
        {
            var fields = GetNoteFields();
            string output = "";
            output += "1. Имя: " + fields.ElementAt(0);
            output += "\n2. Фамилия: " + fields.ElementAt(1);
            output += "\n3. Отчество: " + fields.ElementAt(2);
            output += "\n4. Страна: " + fields.ElementAt(3);
            output += "\n5. Номер телефона: " + fields.ElementAt(4);
            output += "\n6. Дата рождения: " + fields.ElementAt(5);
            output += "\n7. Место работы: " + fields.ElementAt(6);
            output += "\n8. Должность: " + fields.ElementAt(7);
            output += "\n9. Примечания: " + fields.ElementAt(8);
            return output;
        }

        public List<string> GetNoteFields()
        {
            List<string> fields = new List<string>
            {
                Firstname.record,
                Lastname.record,
                SecondName,
                Country.record,
                TelephonNo.record.ToString(),
                DateBirth,
                Organization,
                Position,
                AdditionalInfo
            };
            return fields;
        }

        public void SetNoteField(int indexOfNewEntry, string newField)
        {
            switch (indexOfNewEntry)
            {
                case 0:
                    Firstname = new FirstName(newField);
                    break;
                case 1:
                    Lastname = new LastName(newField);
                    break;
                case 2:
                    SecondName = newField;
                    break;
                case 3:
                    Country = new PersonCountry(newField);
                    break;
                case 4:
                    TelephonNo = new Telephone(Int64.Parse(newField));
                    break;
                case 5:
                    DateBirth = newField;
                    break;
                case 6:
                    Organization = newField;
                    break;
                case 7:
                    Position = newField;
                    break;
                case 8:
                    AdditionalInfo = newField;
                    break;
            }
        }

        public struct FirstName
        {
            public string record;
            public FirstName(string str)
            {
                record = str;
            }
        }
        public struct LastName
        {
            public string record;
            public LastName(string str)
            {
                record = str;
            }
        }
        public struct PersonCountry
        {
            public string record;
            public PersonCountry(string str)
            {
                record = str;
            }
        }
        public struct Telephone
        {
            public long record;
            public Telephone(long str)
            {
                record = str;
            }
        }
    }
}
