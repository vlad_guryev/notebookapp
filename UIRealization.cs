﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace NotebookApp
{
    partial class UserInterface
    {
        static string ValidateEntry(string field, bool isValidated = true)
        {
            string ans = "";
            do
            {
                WriteLine($"Введите {field}:");
                ans = ReadLine();
            } while (ans.Equals("") && isValidated);
            return ans;
        }

        bool ValidateUserReply(ref int idx, string act)
        {
            WriteLine($"Введите номер записи, которую собираетесь {act}" +
               $"\n(подсказка* количество записей в книжке - {book.Notes.Count})");
            int userReply = Int16.Parse(ReadLine());
            int indexOfNote = userReply - 1;

            if (indexOfNote < book.Notes.Count && (indexOfNote > -1))
            {
                idx = indexOfNote;
                return true;
            }
            else
            {
                WriteLine($"Записи под номером {userReply} не существует");
                return false;
            }
        }

        public void Create()
        {
            WriteLine("Заполните обязательные поля новой записи");
            string Name = ValidateEntry("имя");
            string Lastname = ValidateEntry("фамилию");
            string Country = ValidateEntry("страну");

            bool numberParse = Int64.TryParse(ValidateEntry("номер телефона"), out long Telephone);
            while (!numberParse)
            {
                numberParse = Int64.TryParse(ValidateEntry("номер телефона"), out Telephone);
            }

            WriteLine("Заполните необязательные поля новой записи");
            List<string> optionalFileds = new List<string>(5);

            string birthDay = ValidateEntry("дату рождения в формате dd/mm/yy", false);
            optionalFileds.Add(birthDay);
            string fatherName = ValidateEntry("отчество", false);
            optionalFileds.Add(fatherName);
            string organization = ValidateEntry("место работы", false);
            optionalFileds.Add(organization);
            string position = ValidateEntry("должность", false);
            optionalFileds.Add(position);
            string additional = ValidateEntry("примечания", false);
            optionalFileds.Add(additional);

            book.CreateNote(Name, Lastname, Country, Telephone, optionalFileds);
            WriteLine("Запись создана! Вы можете вывести ее на экран, удалить или отредактировать\n\t\t\t\t\t\t\n***");
        }

        public void Edit()
        {
            int indexOfNote = 0;
            if (ValidateUserReply(ref indexOfNote, "редактировать"))
            {
                Note editedNote = book.GetNoteAtIndex(indexOfNote);
                WriteLine("\n" + editedNote.GetFullNoteInfo() + "\n");
                WriteLine("Введите номер строки для редактирования");

                int userReply2 = Int16.Parse(ReadLine());
                while (!((userReply2 > 0) && (userReply2 < 10)))
                {
                    WriteLine("Записи под таким номером не существует, попробуйте еще раз");
                    userReply2 = Int16.Parse(ReadLine());
                }
                userReply2--;

                WriteLine("Введите новое значение для поля");
                string editedField = "";
                switch (userReply2)
                {
                    case 0:
                        editedField = ValidateEntry("имя");
                        break;
                    case 1:
                        editedField = ValidateEntry("фамилию");
                        break;
                    case 2:
                        editedField = ValidateEntry("отчество", false);
                        break;
                    case 3:
                        editedField = ValidateEntry("страну");
                        break;
                    case 4:
                        bool numberParse = Int64.TryParse(ValidateEntry("номер телефона"), out long Telephone);
                        while (!numberParse)
                        {
                            numberParse = Int64.TryParse(ValidateEntry("номер телефона"), out Telephone);
                        }
                        editedField = Telephone.ToString();
                        break;
                    case 5:
                        editedField = ValidateEntry("дату рождения в формате dd/mm/yy", false);
                        break;
                    case 6:
                        editedField = ValidateEntry("место работы", false);
                        break;
                    case 7:
                        editedField = ValidateEntry("должность", false);
                        break;
                    case 8:
                        editedField = ValidateEntry("примечания", false);
                        break;
                }

                book.EditNote(indexOfNote, userReply2, editedField);
                WriteLine("Запись успешно отредактирована!");
            }
        }

        public void Delete()
        {
            int indexOfNote = 0;
            if (ValidateUserReply(ref indexOfNote, "удалить"))
            {
                book.DeleteNote(indexOfNote);
                WriteLine("Запись удалена");
            }
        }
        public void DeleteAll()
        {
            if (book.Notes.Count > 0)
            {
               var count = book.DeleteAllNotes();
               WriteLine($"Удалено записей: {count}");
            }
            else
            {
                WriteLine("Пока нет ни одной записи для удаления");
            }
        }

        public void Show()
        {
            int indexOfNote = 0;
            if (ValidateUserReply(ref indexOfNote, "вывести на экран"))
            {
                Note showedNote = book.GetNoteAtIndex(indexOfNote);
                WriteLine(showedNote.GetFullNoteInfo() + "\n");
            }
        }

        public void ShowAll()
        {
            if (book.Notes.Count > 0)
            {
                int counter = 1;
                foreach (var item in book.Notes)
                {
                    WriteLine($"\nЗапись №{counter++}:\n{item.ToString()}");
                }
            }
            else
            {
                WriteLine("Пока нет ни одной записи для вывода");
            }
        }

        public void Help()
        {
            WriteLine("Список команд:");
            WriteLine("***");
            WriteLine("create - создать запись");
            WriteLine("edit - редактировать запись");
            WriteLine("del - удалить запись");
            WriteLine("delAll - удалить все записи");
            WriteLine("show - вывести запись на экран");
            WriteLine("showAll - вывести все записи на экран");
            WriteLine("help - справка по командам");
            WriteLine("exit - выйти из программы");
            WriteLine("***");
        }

    }
}
